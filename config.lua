require "functions"

Languages={{"en","fr"},{"fr","en"}}

RepoDir = os.getenv('PWD')
Host = "192.168.4.5"
Port = 8080
ServerAdmin = "coutay.ba@anacim.sn"
ServerName = "192.168.4.5"
ServerAlias = "192.168.4.5"
ProxyModule = false -- set to false if you would like to use rewrite rules
ProxyURL = "http://192.168.4.5"
MacSetup = false

if ProxyModule then
ProxyRules = [[
   ProxyPass /SOURCES ]]..ProxyURL..[[/SOURCES
   ProxyPass /expert ]]..ProxyURL..[[/expert
   ProxyPass /ds: ]]..ProxyURL..[[/ds:
   ProxyPass /home ]]..ProxyURL..[[/home
   ProxyPass /openrdf-sesame ]]..ProxyURL..[[/openrdf-sesame
]]
else
ProxyRules = [[
   RewriteRule ^/SOURCES/(.*) ]]..ProxyURL..[[/SOURCES/$1
   RewriteRule ^/expert/(.*) ]]..ProxyURL..[[/expert/$1
   RewriteRule ^/ds:/(.*) ]]..ProxyURL..[[/ds:/$1
   RewriteRule ^/home/(.*) ]]..ProxyURL..[[/home/$1
   RewriteRule ^/openrdf-sesame/(.*) ]]..ProxyURL..[[/openrdf-sesame/$1
]]
end

